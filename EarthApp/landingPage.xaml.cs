﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace EarthApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class landingPage : Page
    {
        public landingPage()
        {
            this.InitializeComponent();
        }

        private void buttonShowPane_Click(object sender, RoutedEventArgs e)
        {
            if (splitview.IsPaneOpen == false)
            {
                splitview.IsPaneOpen = true;
                buttonShowPane.Content = "\uE00E";
            }
            else
            {
                splitview.IsPaneOpen = false;
                buttonShowPane.Content = "\uE00F";

            }


        }

        private void buttonRecycle_Click(object sender, RoutedEventArgs e)
        {
            if (splitview.Content != null)
            {
                ((Frame)splitview.Content).Navigate(typeof(recycleCenters));
            }
        }

        private void buttonHome_Click(object sender, RoutedEventArgs e)
        {
            if(splitview.Content!=null)
            {
                ((Frame)splitview.Content).Navigate(typeof(homePage));
            }
        }

        private void buttonDevices_Click(object sender, RoutedEventArgs e)
        {
            if (splitview.Content != null)
            {
                ((Frame)splitview.Content).Navigate(typeof(addDevice));
            }
        }

        private void buttonSettings_Click(object sender, RoutedEventArgs e)
        {
            if (splitview.Content != null)
            {
                ((Frame)splitview.Content).Navigate(typeof(Settings));
            }
        }

        private void buttonPlant_Click(object sender, RoutedEventArgs e)
        {
            if (splitview.Content != null)
            {
                ((Frame)splitview.Content).Navigate(typeof(plantTree));
            }
        }

        private void buttonGlobal_Click(object sender, RoutedEventArgs e)
        {
            if(splitview.Content == null)
            {
                ((Frame)splitview.Content).Navigate(typeof(globalStats));
            }
        }
    }
}
