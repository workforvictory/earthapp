﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
namespace EarthApp
{
    public sealed partial class addDevice : Page
    {
        GenericCommand _command;
        public addDevice()
        {
            this.InitializeComponent();
            this.Loaded += AddDevice_Loaded;
            _command = new GenericCommand();
            _command.DoSomething += _command_DoSomething;
        }

        async  private void _command_DoSomething(string command)
        {
            if(command.ToLower()=="Add Device")
            {
                
                MessageDialog md = new MessageDialog("Device Add popup");
                await md.ShowAsync();
            }
        }

        private void AddDevice_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = _command;
          //  throw new NotImplementedException();
        }

        

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DeviceDetails), null);
        }
    }
}
